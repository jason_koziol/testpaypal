<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test PayPal Adaptive</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
	<form method="POST" action="Pay" target="theframe">
		Currency:
		<input type="text" name="currencyCode" value="USD" />
		<br/>
		Action type:
		<select name="actionType">
			<option value="">--Select a value--</option>
			<option value="PAY">Pay</option>
			<option value="CREATE">Create</option>
			<option value="PAY_PRIMARY">Pay Primary</option>
		</select>
		<br/>			
		Cancel URL:
		<input type="text" name="cancelURL" value="http://localhost:8080/TestPayPal/testpaypaladaptresponse.jsp" />
		<br/>
		Return URL:
		<input type="text" name="returnURL" value="http://localhost:8080/TestPayPal/testpaypaladaptresponse.jsp" />
		<br/>
		Amount:
		<input type="text" name="amount" value="2.00" />
		<br/>
		Recipient Email:
		<input type="text" name="mail" value="" />
		
		<select name="setPrimary">
				<option value="">--Select a value--</option>
				<option value="false">Not a Primary Receiver</option>
				<option value="true">Primary Receiver</option>
		</select>
		<br/>
		<input type="submit" name="PayBtn" value="Pay" />
	</form>
	<br/>
	<button onclick="setsource()" value="set source">set source</button>
	<br/>
	<iframe id="theframe" name="theframe" src="http://www.google.com" width="500" height="500">
	</iframe>	
<script type="text/javascript">
function setsource()
{
	$('iframe#theframe').attr('src', "http://www.42andpark.com");

    $('iframe#theframe').load(function() {
        alert(this);
    });
}

$(function(){
    $('iframe#theframe').load(function() {
        alert(this);
    });
    
	//$('iframe#theframe').attr('src', "http://www.paypal.com");
});
</script>
</body>
</html>