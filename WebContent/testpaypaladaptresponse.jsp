<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test PayPal Adaptive - Response</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
	<table>
		<%
			Map map = (Map) session.getAttribute("map");
			Iterator iterator = map.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
		%>

		<tr>
			<td><%=mapEntry.getKey()%></td>
			<td>:</td>
			<td><div id="<%=mapEntry.getKey()%>"><%=mapEntry.getValue()%></div></td>
		</tr>

		<%
			}
		%>
	</table>
	<textarea rows="10" cols="100"><%=session.getAttribute("lastResp")%></textarea>
	<br/>
	<button onclick="setsource()" value="set source">set source</button>
	<br/>
	<iframe id="theframe" src="http://www.google.com" width="500" height="500">
	</iframe>	
<script type="text/javascript">
function setsource()
{
	console.log($("[id='Redirect URL'] a").attr('href'));
	$('iframe#theframe').attr("src", $("[id='Redirect URL'] a").attr('href'));

    //$('iframe#theframe').load(function() {
     //   alert(this);
    //});
}

$(function(){
    $('iframe#theframe').load(function() {
        alert(this);
    });
    
	//$('iframe#theframe').attr('src', "http://www.paypal.com");
});
</script>
</body>
</html>