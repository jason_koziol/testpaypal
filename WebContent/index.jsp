<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PayPal Credit Card Processing Test</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js"></script>
</head>
<body style="font-family:verdana;font-size:12px;background-color:#ffffbb;">
<h2>PayPal Credit Card Processing Test</h2>
<table>
	<tr>
		<td>
		<table>
			<tr><td><label for="item">Item</label></td><td><span id="item" style="font-weight:bold;">Yellow Widget SKU#04567209</span></td></tr>
			<tr><td><label for="fullname">Name</label></td><td><input id="fullname" type="text" size="20" value="Joe Shopper"></input></td></tr>
			<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
			<tr><td><label for="total">Total $</label></td><td><input id="total" type="text" size="10" value="7.00"></input></td></tr>
			<tr><td><label for="cctype">Type</label></td><td><select id="cctype"><option selected>Visa</option></select>
			<tr><td><label for="ccnum">Number</label></td><td><input id="ccnum" type="text" size="16" value="4417119669820331"></input></td></tr>
			<tr><td><label for="cvv">CVV</label></td><td><input id="cvv" type="text" size="3" value="874"></input></td></tr>
			<tr><td><label for="date">Exipration</label></td>
				<td>
					<span id="date">
						<input id="month" type="text" size="2" value="11"></input>
						<input id="year" type="text" size="4" value="2018"></input>
					</span>
				</td>
			</tr>
			<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
			<tr><td><label for="address">Address</label></td><td><input id="address" type="text" size="20" value="52 N Main ST"></input></td></tr>
			<tr><td><label for="city">City</label></td><td><input id="city" type="text" size="20" value="Johnstown"></input></td></tr>
			<tr><td><label for="state">State</label></td><td><input id="state" type="text" size="2" value="OH"></input></td></tr>
			<tr><td><label for="zip">Zip</label></td><td><input id="zip" type="text" size="5" value="43210"></input></td></tr>
			<tr><td colspan="2"><button type="submit" id="paynow">Pay Now!</button></td></tr>
		</table>
		</td>
		<td style="vertical-align:top;">
			<img width="150px" height="150px" style="border:1px solid black;"src="http://www.homedepot.com/catalog/productImages/300/53/539c890e-1db2-46d4-acae-40d0a42816ac_300.jpg" />
			<pre style="text-align:left;" id="result">
{	
	"result":	"ersdfesf",
	"status":	"paid"
}
			</pre>
		</td>
	</tr>
</table>
<script>
$(function(){
	$("#paynow").click(function(e){
		var query = "ccnum=" + $("#ccnum").val() + "&total=" + $("#total").val();
		$.ajax({
			type: "GET",
			url: "paypalccpmt?" + query,
			contentType: "application/x-www-form-urlencoded",
			dataType: "json", 
			success: function(data){
				console.log("success: ", data);
				$("#result").html(JSON.stringify(data));
			},
			error: function(err){
				console.log(err);
				$("#result").html(err);
			}
		});
		e.preventDefault();
	});
});
</script>
</body>
</html>