package com.forty2andpark.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.paypal.api.payments.Address;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.CreditCard;
import com.paypal.api.payments.AmountDetails;
import com.paypal.api.payments.FundingInstrument;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.Transaction;
//import com.paypal.api.payments.util.GenerateAccessToken;
import com.paypal.core.rest.OAuthTokenCredential;
import com.paypal.core.rest.APIContext;
//import com.paypal.core.rest.PayPalRESTException;
import com.paypal.core.rest.PayPalRESTException;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class PayPalPaymentCreditCard
 */
@WebServlet(
		description = "paypal cc pmt", 
		urlPatterns = { 
				"/PayPalPaymentCreditCard", 
				"/paypalccpmt"
		})
public class PayPalPaymentCreditCard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayPalPaymentCreditCard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		Address billingAddress = new Address();
		billingAddress.setCity("Johnstown");
		billingAddress.setCountryCode("US");
		billingAddress.setLine1("52 N Main ST");
		billingAddress.setPostalCode("43210");
		billingAddress.setState("OH");

		// ###CreditCard
		// A resource representing a credit card that can be
		// used to fund a payment.
		CreditCard creditCard = new CreditCard();
		creditCard.setBillingAddress(billingAddress);
		creditCard.setCvv2("874");
		creditCard.setExpireMonth("11");
		creditCard.setExpireYear("2018");
		creditCard.setFirstName("Joe2");
		creditCard.setLastName("Shopper2");
		//creditCard.setNumber("4417119669820331");
		creditCard.setNumber(req.getParameter("ccnum"));
		creditCard.setType("visa");

		// ###Details
		// Let's you specify details of a payment amount.
		AmountDetails details = new AmountDetails();
		details.setShipping("2");
		Integer subTotal = Integer.parseInt(req.getParameter("total")) - 3;
		details.setSubtotal(subTotal.toString());
		details.setTax("1");

		// ###Amount
		// Let's you specify a payment amount.
		Amount amount = new Amount();
		amount.setCurrency("USD");
		// Total must be equal to sum of shipping, tax and subtotal.
		Integer total = Integer.parseInt(req.getParameter("total"));
		amount.setTotal(total.toString());
		amount.setDetails(details);

		// ###Transaction
		// A transaction defines the contract of a
		// payment - what is the payment for and who
		// is fulfilling it. Transaction is created with
		// a `Payee` and `Amount` types
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction
				.setDescription("This is the payment transaction description.");

		// The Payment creation API requires a list of
		// Transaction; add the created `Transaction`
		// to a List
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		// ###FundingInstrument
		// A resource representing a Payeer's funding instrument.
		// Use a Payer ID (A unique identifier of the payer generated
		// and provided by the facilitator. This is required when
		// creating or using a tokenized funding instrument)
		// and the `CreditCardDetails`
		FundingInstrument fundingInstrument = new FundingInstrument();
		fundingInstrument.setCreditCard(creditCard);

		// The Payment creation API requires a list of
		// FundingInstrument; add the created `FundingInstrument`
		// to a List
		List<FundingInstrument> fundingInstrumentList = new ArrayList<FundingInstrument>();
		fundingInstrumentList.add(fundingInstrument);

		// ###Payer
		// A resource representing a Payer that funds a payment
		// Use the List of `FundingInstrument` and the Payment Method
		// as 'credit_card'
		Payer payer = new Payer();
		payer.setFundingInstruments(fundingInstrumentList);
		payer.setPaymentMethod("credit_card");

		// ###Payment
		// A Payment Resource; create one using
		// the above types and intent as 'sale'
		Payment payment = new Payment();
		payment.setIntent("sale");
		payment.setPayer(payer);
		payment.setTransactions(transactions);
		
		String result = "";

		try {
			//payment.initConfig(new File("C:\\Users\\Student\\workspace42\\TestPayPal\\src\\sdk_config.properties"));
			// ###AccessToken
			// Retrieve the access token from
			// OAuthTokenCredential by passing in
			// ClientID and ClientSecret
			// It is not mandatory to generate Access Token on a per call basis.
			// Typically the access token can be generated once and
			// reused within the expiry window
			
			//com.paypal.core.ConfigManager.getInstance().load(new FileInputStream("C:\\Users\\Student\\workspace42\\TestPayPal\\src\\sdk_config.properties"));
			
			OAuthTokenCredential oauthCred = new OAuthTokenCredential("AVNv3xCIWeMRmFqTYF_sVyvtkwg2ejNEA_ya3MJsKKRjoyma9xwxqi6o5wz2",
					"EGBoBxDxrQpAvDyeAdAxu-e1a8cvK8Zx3061yJOUP3zvfAuVwFXHwytT6pIN"); 
			
			String accessToken =  oauthCred.getAccessToken();

			// ### Api Context
			// Pass in a `ApiContext` object to authenticate 
			// the call and to send a unique request id 
			// (that ensures idempotency). The SDK generates
			// a request id if you do not pass one explicitly. 
			APIContext apiContext = new APIContext(accessToken);
			// Use this variant if you want to pass in a request id  
			// that is meaningful in your application, ideally 
			// a order id.
			/* 
			 * String requestId = Long.toString(System.nanoTime();
			 * APIContext apiContext = new APIContext(accessToken, requestId ));
			 */
			
			// Create a payment by posting to the APIService
			// using a valid AccessToken
			// The return object contains the status;
			Payment createdPayment = payment.create(apiContext);
			req.setAttribute("response", Payment.getLastResponse());
			Logger.getLogger(this.getClass().getName()).info("Created payment with id = " + createdPayment.getId()
					+ " and status = " + createdPayment.getState());
			result = createdPayment.getId();
		} catch (PayPalRESTException e) {
			req.setAttribute("error", e.getMessage());
			result = e.getMessage();
		}

		req.setAttribute("request", Payment.getLastRequest());
		
		res.setHeader("Access-Control-Allow-Origin", "*");
		//response.getOutputStream().println("PayPal CC Payment Transaction Test<br/>");
		res.setContentType("application/json");
		res.getOutputStream().println("{\n\t\"result\": \"" + result + "\"\n}");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
